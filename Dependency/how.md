# Using Dependency Injection

**Dependency Injection** allow us to write code more easier and flexible.

Here the example how to use dependency injection in [Merapi](../MerapiService/what.html).

#### Step 1: Create Component File
After we [installed Merapi](../MerapiService/create.html), let's we create a [component](../Component/what.html) named *"sumComponent"* and *"doubleComponent"*.

![Local Image](../gitbook/images/Selection_018.png)

We done with the component, the next thing we should do is initialize the components in config file.

```javascript
// config.json
config: {
		"init_number" : 2,
		"service" :{
			"host":"localhost",
			"port": 3002,
			"name": "merapiTest",			
			"version" : "1.0.0"
		},
		"components":[
			{"name":"sumComponent", "com":"SumComponent"},
			{"name":"doubleComponent", "com":"DoubleComponent"}
		]
	}
```

#### Step 2: Fill Out Component File
Copy the code below and paste in *sum_component.js*
```javascript
// This is sum_component.js 

var async = require('asyncawait/async');
var await = require('asyncawait/await');

// called config file
module.exports = function(config){
	//we get config("init_number") in config file (config.json) 
	var initNumber = config("init_number");
	var sum = async(function(number){
		return await(initNumber+number);
	});

	return {
        sum: sum 
    };
}
```

Copy the code below and paste in *double_component.js*
```javascript
// This is double_component.js 

var async = require('asyncawait/async');
var await = require('asyncawait/await');

// called sumComponent
module.exports = function(sumComponent){

	var double = async(function(number){
		return await(sumComponent.sum(number))*2;
	});

	return {
        double: double
    };
}
```

#### Step 3: Create File test.js and Run the Code

How we can run these components? We must fill out test.js which we created [before](../MerapiService/create.html).

Copy the code below and paste in *test.js*
```javascript
// test.js
var merapi = require('merapi');
var async = require('asyncawait/async');
var await = require('asyncawait/await');
var config = require('./config.json');

var container = merapi({
    basepath: __dirname,
	config: config
});

async(function(){
	await(container.initialize());
	var sumComponent = await(container.resolve("sumComponent"));
	var doubleComponent = await(container.resolve("doubleComponent"));
	console.log("The result of sumComponent is "await(sumComponent.sum(4)));
	console.log("The result of doubleComponent is "await(doubleComponent.double(4)));
})();
```

And if we run test.js, the result is 
```javascript
$ node test.js
The result of sumComponent is 6
The result of doubleComponent is 12
```

If we look at the step 2, we found that when sumComponent want to call *"init_number"* from *config.json*, this component don't need to initialize *config.json* first. In sumComponent file, if we need to use config, we just called it as a parameter constructor. **Magically, we dont need to do "new" again to initialize config as a dependency**. 

We has config as our dependency, so we can call *"init_number"* in config and use it in our function. 
So does with doubleComponent, this component doesn't need to initialized sumComponent before call the function, it's mean doubleComponent have sumComponent as dependency.
 
This is what we called **dependency injection**.

 


