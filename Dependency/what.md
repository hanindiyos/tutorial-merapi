# What is Dependency Injection

Before we starting to create [Merapi Service](../MerapiService/index.html), we should know about the powerful feature of [Merapi](../MerapiService/what.html). This feature called *"Dependency Injection"*.

**Dependency Injection** is a software-design pattern in which an object is given its dependencies rather than the object creating them itself. **Dependency Injection** have responsibility to collaborated a [component](../Component/index.html) to another component as dependencies. With dependency injection we don't need to know how the component can communicate with other, or where is the location of the other component. We just needed to write the dependencies that we want to called. Dependency Injection makes it simple for us to manage dependencies between [objects](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object). So, Dependency injection is simply about injecting the dependencies into a [component](../Component/index.html) from the outside. 

This is an example code without dependency injection in [Java](https://docs.oracle.com/javase/tutorial/java/) 

```javascript

public class City {

    City() {
        this.postalcode = new PostalCode();
	// need to create an object
    }

    public String getPost() {
        return "The Postal Code is " + postalcode.getNum();
    }
}
```

The **City class** has a dependency on **PostalCode Class**. The code above show us that we need to create object with initialize it first. What is we want to change the dependency? Lets say we don't need *PostalCode class* again, and we want to change to the other better class. If that happends, it might causes many problem for us. Because we don't just need to change one syntax or function, maybe we must to change many function on it. 

#### Take a look another example with [Node Js](https://nodejs.org/en/).
Let's say, we have file named *hello.js*.
```javascript
// hello.js
exports.hello = function() {
    return console.log('Hello World!');
}
```
And we have another file name *hello_world.js*.
helloWorld.js will call function in another file, so we need to initialize the other file as variable.

```javascript
// hello_world.js
var helloWorld = require('./hello.js');
// called a function from hello.js 
helloWorld.hello();
```

And if we run hello_world.js, the result is 
```javascript
$ node helloWorld.js
Hello World!
```

Look at the code above, we found that if we want to call function from another file or component, **we must initialize the component first**. If we don't initialize the [component](../Component/index.html), javascript can't call the function in these component. 

So in the next section, we will learn how to use [dependency injection](how.html) in our application.
