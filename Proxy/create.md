# How to Create Proxy 

Now we want to create proxy from another service.
We will called another service name *getHello*, *getTest*, and *getFinal* in service file named *hello.js* as a proxy named *testProxy*.

Here is the step by step to create proxy 

#### Step 1: Create Service File 
First, we need to create service file named *hello.js*

```javascript
// hello.js 
// hello.js is in service1
var async = require('asyncawait/async');
var await = require('asyncawait/await');
module.exports = function(config, helloWorld, testWorld, finalWorld){
	return {
        // getHello service 
		"/get_hello": async(function(world){
          	return helloWorld.hello(world);
      	}),
        // getTest service
      	"/get_test": async(function(world){
          	return testWorld.test(world);
      	}),
        // getFinal service 
      	"/get_final": async(function(world){
          	return finalWorld.final(world);
      	})
	}
}
```

#### Step 2: Setting Configuration
Next step, after we make a service, we should initialize the component to [config file](../Config/file.html)

```javascript
// config.json
{
    "components": [
        {"name":"helloWorld", "com":"HelloWorld"},
	{"name":"testWorld", "com":"TestWorld"},
	{"name":"finalWorld", "com":"FinalWorld"}
    ],    
    "service": {
        "host": "localhost",
        "port": 3002,
        "name": "service1",
        "version": "1.0.0"
    }
}
```

#### Step 3: Create File test.js and Run the Code
we want to called *getHello*, *getTest*, and *getFinal* in another service.
So we must encapsulated the another service as a proxy in our service.

```javascript
// test.js
var merapi = require('merapi');
var async = require('asyncawait/async');
var await = require('asyncawait/await');
var config = require('./config.json');
var service1 = merapi({
        basepath: __dirname,
        config: config
    });    
var service2 = merapi({
    basepath: __dirname,
	config: {
		service:{
			"host": "localhost",
			"port": 3001,
			"name": "merapiTest",			
			"version" : "1.0.0"
		},
		components:[
            // encapsulated the services as testProxy
	    // when we use testProxy, we pretending to located in 3002
			{name:"testProxy", proxy:"http://localhost:3002"}
		]
	}
});

var proxy = null;
async(function(){
	await(hello.start(require('./hello.js')));
	await(container.start(function(testProxy) {
            proxy = testProxy;
            return {};
        }));
    // called another services
	await(proxy.getHello("world"));
	await(proxy.getTest("test"));
    await(proxy.getFinal("final"));
})();

```

And if we run test.js, the result is 
```javascript
$ node test.js
"Hello world"
"Hello test"
"Hello final"
```
And now we know about **Proxy** and how to **create a Proxy**.

