# What is Proxy

We ever found *the proxy* in the previous section. So what is **proxy**?
When we used [microservice design pattern](http://martinfowler.com/articles/microservices.html), it will allow us to call another service in our service. And it can be happens if you use **proxy**. It can be helped us to developed our application to much easier and also it can be debugged easier. 

It is like **proxy server**, when we want to call another service in different location, we need to access the service by pretending to be located in service's location which we want to call. We know that, each service will be running in different location (host). So proxy is intermediary when you need to access another service in your service with pretending to be another service.
