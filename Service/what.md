# What is Service 

When we developing a [server-sided application](http://programmers.stackexchange.com/questions/171203/what-are-the-differences-between-server-side-and-client-side-programming), It must support a variety of different client request. So to handle that we offered the application architecture called microservices architecture. **Microservices architecture** is one of software system where comprised of a number of independently deployable services, each with a limited scope of responsibility. 

**Services** communicate using either synchronous protocols such as HTTP/REST or asynchronous protocols. **Services** are developed and deployed independently of one another. So each developer in team can developed the service independently. Each services can called another service. 


![Local Image](../gitbook/images/Selection_023.png)
