# How to Create Service
And how we can create a web service in Merapi?.
Here step by step to create service.


#### Step 1: Create Component File 
We need to create a component named *sumComponent.js*.
Copy and paste the code below.

```javascript
var async = require('asyncawait/async');
var await = require('asyncawait/await');
module.exports = function(config){
	var initNumber = config("init_number");
	var sum = async(function(number){
		return await(initNumber+number);
	});
	return {
        sum: sum
    };
}
```
#### Step 2: Setting Configuration
Next step, after we make new component, we should initialize component to [config file](../Config/file.html)

```javascript
// config.json
{
    "init_number" : "1",
    "components": [
        {"name":"sumComponent", "com":"sumComponent"}
    ],    
    "service": {
        "host": "localhost",
        "port": 4000,
        "name": "service1",
        "version": "1.0.0"
    }
}
```

#### Step 3: Create Service File 
Next, create a service file name *service.js*
Copy and paste the code below.

```javascript
// service.js contain get_sum service which other service called it
// service.js located in service1
var async = require('asyncawait/async');
var await = require('asyncawait/await');
module.exports = function(config, sumComponent){
	return {
		"/get_sum": async(function(number){
          	return sumComponent.sum(number);
      	})
	}
}
```

We created a service name get_sum service, which will get the return the value of sumComponent.

#### Step 4: Create File test.js in another service and Run the Code

Last step, run these component with create test file.
Copy the code below and paste in *test.js*

```javascript
var merapi = require('merapi');
var async = require('asyncawait/async');
var await = require('asyncawait/await');
var config = require('./config.json');
// service1 is external service
var service1 = merapi({
        basepath: __dirname,
        config: config

    });
// service2 is our service
var service2 = merapi({
    basepath: __dirname,
	config: {
		service:{
			"host": "localhost",
			"port": 3001,
			"name": "service2",			
			"version" : "1.0.0"
		},
		components:[
		//encapsulated the service1 as testProxy
			{name:"testProxy", proxy:"http://localhost:4000"}
		]
	}
});
var proxy = null;
async(function(){
	await(service1.start(require('./service.js')));
	await(service2.start(function(testProxy) {
            proxy = testProxy;
            return {};
        }));
	await(proxy.getSum(9));

})();

```

And if we run test.js, the result is 
```javascript
$ node test.js
10
```

Now, we can **create a service** and use the service in another service. 
