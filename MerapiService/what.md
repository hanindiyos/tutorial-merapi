# What is Merapi

Merapi is a powerful web application Framework, for use in building dynamic web application with NodeJS. 

In Merapi, you will found a magical feature which will help you a lot while developing an application. This feature called dependency injection. Furthermore, with Merapi you will learn a lot of new things when build an application.

Before you starting Merapi, you should have nodes in your PC.
You can download NodeJS here
https://nodejs.org/en/download/

