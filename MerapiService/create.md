# How To Install Merapi

Before we create Merapi Service, we need to prepare and setting some configuration and installation processing.

This is step by step to install merapi service.

#### Step 1: Create a folder for Merapi 
First, We need to create a folder as our service. 
As an example we create an empty folder name **"merapi-test"**

![Local Image](../gitbook/images/Selection_008.png) 

#### Step 2: Create some file in Master folder 
We need create some file such as 
1. *config.json*
2. *package.json*
3. *index.js*
4. *test.js*
 
![Local Image](../gitbook/images/Selection_009.png)

let the files empty, we will fill the contain later in the next section.


#### Step 3: NPM Init
1. Open your **command line** and copy the text below
```javascript
npm init
```

2. After we do NPM init, the command line will show you 
```javascript
name: (merapi-test) 
version: (1.0.0) 
description: 
entry point: (index.js) 
test command: 
git repository: 
keywords: 
author: 
license: (ISC) 
```

Its optional, if you don't want to fill out the form, you can skip it by push the enter button.
If you want to know more about *package.json* , you can click [here](https://docs.npmjs.com/files/package.json).

3. Finally, our *package.json* will be contain like this below
```javascript
// package.json
{
  "name": "merapi-test",
  "version": "1.0.0",
  "description": "",
  "main": "index.js",
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1"
  },
  "author": "",
  "license": "ISC"
}
```

#### Step 4: NPM Install Merapi
Next, we should install [merapi](what.html) in our service folder.
You should open your command line and copy the syntax below.
```javascript
 npm install git+ssh://git@bitbucket.org/yesboss/merapi.git --save
```

#### Step 5: NPM Install Asyncawait
Last, we need to install [asyncawait](../Asyncawait/index.html) in our service folder.
Open your command line and copy the syntax below.
```javascript
 npm install async --save
```

After we installing Merapi and Asyncawait, our package.json will be update like this 

![Local Image](../gitbook/images/Selection_014.png)


Now, we have installed Merapi in our folder, the next thing we can create merapi service.
Before create a service, we must understand many material which construct the Merapi.



