# What is Hook

**Webhooks** are user-defined HTTP callbacks. A webhook (also called a web callback or HTTP push API) is a way for an app to provide other applications with real-time information. A webhook delivers data to other applications as it happens, meaning you get data immediately. Its not like API, when u must to selective a data frequently to get the right data. Also, Webhook listen for events from your devices. When you send a matching event, the hook will send a request to your web application with all the details. Webhooks listen for events from your devices and will make requests based on those events. 

[Prefinery](https://www.prefinery.com/) is the one application who we can integrate webhook on their apps.
https://www.prefinery.com/api/v2/docs/testers

If u want to access data from prefinery to your application, you can used webhook feature.
Webhook on prefinery will send you data to your application while you have an HTTP request to prefinery.

![Local Image](../gitbook/images/Hook.PNG)

