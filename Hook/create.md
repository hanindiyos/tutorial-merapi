# How to Create Hook

If we want to request webhook in another application we must look at the type of request that we want.

Example, we want to get data from prefinery webhook and we will request HTTP get and receive the data. 

#### Step 1: Setting prefinery configuration in config file
```javascript
// config.json
{
		"hello":"Hello",
		"a": {"b":1},
		"service":{
			"host": "localhost",
			"port": 3002,
			"name": "merapiTest",			
			"version" : "1.0.0"
		},
		"prefinery": {
			// prefinery will give us apikey to access
			"beta": 7369,
			"account": "yesboss",
			"apikey": "bmxwi8DxnT64BPMppzZx"
		},
		"components":[
			{"name":"helloWorld", "com":"HelloWorld"},
			{"name":"testWorld", "com":"TestWorld"},
			{"name":"finalWorld", "com":"FinalWorld"},
			{"name":"hookWorld", "com":"HookWorld"}
		]
	}
```

#### Step 2: Create Hook Component
```javascript
// hook_component.js
var async = require('asyncawait/async');
var await = require('asyncawait/await');
var request = require('request-promise');
module.exports = function(config){
	var account = config("prefinery.account");
	var id = config("prefinery.beta");
	var apikey = config("prefinery.apikey");	
	return {
		hook: async(function() {
            try {
                return console.log(await(request({
                    uri: "https://"+ account +".prefinery.com/api/v2/betas/"+id+".json?api_key="+apikey,
                    method: 'GET',
                    json: true
                })));
            } catch(e) {
                return null;
            }
        }),
	}
}
```
#### Step 3: Call the Hook 
```javascript
// test.js 
var test = null;
async(function(){
	await(hello.start(require('./service.js')));
	await(container.start(function(testProxy) {
            test = testProxy;
            return {};
        }));
	await(test.getHello("world"));
	await(test.getTest("test"));
    await(test.getFinal("final"));
    await(test.getHook());

})();
```


