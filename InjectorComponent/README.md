# Injector Component


##### What is Injector? Why we create Injector?

**Injector** is a component which can injecting or constructing the service object to [dependency injection](../Dependency/what.html) container. Injector is needed for dependency injection. This injector also responsibility of providing its dependencies.

**Injector** can be initialized in *lib/injector.js*.
Look at the file *injector.js* we can look some function which Injector have. These function are *register*, *loadComponent*, *resolve*, *dependencies*, *dependencyName* and *execute*.


So in this chapter, We will learn about resolve and execute function :
###### [6.1. What is Resolve](resolve.html)
###### [6.2. What is Execute](execute.html)

