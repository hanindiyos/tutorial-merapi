# Excecute

**Excecute** function is the function which used to excecute a function using [dependency injection](../Dependency/what.html).
Same as with [resolve](resolve.html) function, [execute](execute.html) function will call the container also and find the dependencies of component. But the different is the injector execute will be direct to execute the function without initialize the component as a variable.
So in some cases, the injector execute will be faster than the injector resolve. 


Here is example with injector execute. 
This code will look at container and **directed to execute the function** of component.
We still use *helloWorld*, *testWorld*, and *finalWorld* components (look at the [previous page](resolve.html)). 

```javascript
// test.js
var merapi = require('merapi');
var async = require('asyncawait/async');
var await = require('asyncawait/await');
var container = merapi({
    basepath: __dirname,
	config: {
		hello:"Hello",
		a: {b:1},
		service:{
			name:"merapiTest"
		},
		components:[
			{name:"helloWorld", com:"HelloWorld"},
			{name:"testWorld", com:"TestWorld"},
			{name:"finalWorld", com:"FinalWorld"}
		]
	}
});
async(function(){
	await(container.initialize());
	container.execute(function(testWorld, helloWorld, finalWorld){	
			helloWorld.hello("world");
			testWorld.test("test");
			finalWorld.final("final");
		})
})();
```

And if we run *test.js*, the result is same as with the *test.js* which called **resolve** function in the [previous page](resolve.html). 
```javascript
$ node test.js
"Hello world"
"Hello test"
"Hello final"
```
