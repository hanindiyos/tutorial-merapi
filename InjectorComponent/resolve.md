# Resolver

**Resolve** function is the function which used to resolve a component by name. It will be responsibility for *constructing dependencies* the component. 

Imagine that there are a container. And the container must be contain some component. In this container, each of component should be initialize the dependencies which they have. 

So with **resolve a component**, the intrepeter will look at the container and find the dependencies and make them return an object, so the [component](../Component/what.html) can used them. 
 

Let us try some example here.
We have three [components](../Component/what.html) that we want to used here.
helloWorld component, testWorld component, and finalWorld component

![Local Image](../gitbook/images/Selection_005.png)

This code below is *helloWorld* component.

HelloWorld component needed dependency from config file.
And helloWorld component will return object function named hello function.

```javascript
// This is hello_world.js
module.exports = function(config){	
	return {
		hello:function(world){
			console.log(config("hello")+" "+world);
		}
	}
}
```

This code below is *testWorld* component.

TestWorld component needed dependency from helloWorld component.
And testWorld component will return object funtion named test function.

```javascript
// This is test_world.js
module.exports = function(helloWorld){	
	return {
		test:function(world){
			helloWorld.hello(world);
		}
	}
}
```

This code below is *finalWorld* component.

FinalWorld component needed dependency from testWorld component.
And finalWorld component will return object function named final function.

```javascript
// This is final_world.js
module.exports = function(testWorld){	
	return {
		final:function(world){
			testWorld.test(world);
		}
	}
}
```


And we will call the components with **injector resolve** in *test.js*

```javascript
// test.js
var merapi = require('merapi');
var async = require('asyncawait/async');
var await = require('asyncawait/await');
var container = merapi({
    basepath: __dirname,
	config: {
		hello:"Hello",
		a: {b:1},
		service:{
			name:"merapiTest"
		},
		components:[
			{name:"helloWorld", com:"HelloWorld"},
			{name:"testWorld", com:"TestWorld"},
			{name:"finalWorld", com:"FinalWorld"}
		]
	}
});
async(function(){
	await(container.initialize());
	var helloWorld = await(container.resolve("helloWorld"));
	var testWorld = await(container.resolve("testWorld"));
	var finalWorld = await(container.resolve("finalWorld"));
	helloWorld.hello("world");
	testWorld.test("test");
	finalWorld.final("final");
	
})();
```
And if we run test.js, the result is 
```javascript
$ node test.js
"Hello world"
"Hello test"
"Hello final"
```



