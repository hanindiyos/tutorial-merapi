# Model And Controller

In this chapter we will learn about Model and Controller in [Merapi Framework](../MerapiService/what.html).

###### [7.1 What is Model?](model.html)
###### [7.2 What is Controller?](controller.html)



![Local Image](../gitbook/images/MVC.png)
