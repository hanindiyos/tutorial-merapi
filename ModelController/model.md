# What is Model

We use [MongoDB](https://docs.mongodb.org/manual/) as our database. **MongoDB** is object-oriented database system. Because we use Node Js in our application, we will familiar too with mongoose. **Mongoose** is a Node.js library that provides MongoDB object mapping similar to ORM with a familiar interface within Node.js .

In mongoose,  a **Model** defines a programming interface for interacting with the database (read, insert, update, etc). And what is **shcema?**, in mongoose schema represents the structure of document. It's a way to express expected properties and values as well as constraints and indexes. So a model provides functionality like *"Add a new document to collection"* and a schema will answer *"what will the data look like?"*. 


### How create a Model
Imagine that we would to build a system information for library. So before we do an implementation, we need to create a library database. To make this easier we should chunk the database into a table.
Their table are book, librarian, customer, and order.

In this section we will create a model file named *book.js*.

#### Step 1: Create Models Folder
First, we need to create our model file in folder Models.

![Local Image](../gitbook/images/Selection_020.png)

#### Step 2: Create Models File
Next, create model file named *"book.js"* and copy the code below

```javascript
// Models/book.js
var Schema = require('mongoose').Schema;

module.exports = function() {
    return new Schema({        
        book_isbn: {type: String, required: true},        
        book_title: {type: String},
        book_author: {type: String},
        book_publisher: {type: String},
        book_status: {type: String},
        book_category: {type: String},
        book_year: Number,
        book_quantity:Number
    }); 
};
```
The table of book contains many attribute such as isbn (primary key), title, author, publisher, status, category, year of published, and quantity.

#### Step 3: Setting Configuration
After we create a model, we must adding the model file to configuration file in *config.json*

```javascript
// config.json
{   
    "models": [
        "Book"
    ],    
    "service": {
        "host": "localhost",
        "port": 3002,
        "name": "yb-test",
        "version": "1.0.0"
    }
}

```
 
Now, we know about **model** and we can **create a model**.
In the next section, we will learn about [controller](controller.html). 

