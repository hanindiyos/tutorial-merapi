# What is Controller

**Controller** represents a method that connected client (view) and service. The controller is the input mechanism of the user interface, which is often tangled up in the view. The controller can handle the request by client, and after that the client can get the response from controller. And **controller** will listen some kind of interaction or event by client. 

### How create a Controller
Lets say, we want to register new book to our Library System. 
In the view Classes, the client make a request to create new book. So we must create a controller which can handle the request and will return the response.

In this section we will create a controller file named *register_controller.js*.

#### Step 1: Create Controllers Folder
First, we need to create our controller file in Controllers Folder.

![Local Image](../gitbook/images/Selection_021.png)

#### Step 2: Create Controllers File
Next, create controller file named *"register_controller.js"* and copy the code below

```javascript
// Controllers/register_controller.js 

var async = require('asyncawait/async');
var await = require('asyncawait/await');
module.exports = function(config, Book) {
    return {
        // if the user post something on the form in view 
        "POST /": async(function(req, res) {            
            var data = req.body;            
            var isBook = await(Book.findOne({book.isbn:data.isbn));
            if (!isBook) {
                // create new book in database
                var regbook = await(Book.create({
                     book_isbn: data.isbn,        
                     book_title: data.title,
                     book_author: data.author,
                     book_publisher: data.publisher,
                     book_status: 'available',
                     book_category: data.category,
                     book_year: data.year,
                     book_quantity:1
                }));                
                res.json({
                    status: "success",
                    data: {
                        bookTitle: regbook.title,
                        bookAuthor: regbook.author
                    }
                });                
                return;
            }            
            res.json({
                status: "error",
                error: "Book already registered"
            });
        })
    };
```

In the code above, we can find that the controller used the component Model with dependency injection pattern. With called the Model, the controller can manipulating the model in the method which we can initialized it. 

#### Step 3: Setting Configuration

After we create a controller, we must adding the controller file to configuration file in config.json

```javascript

// config.json
{
    "controllers": [
	{"name":"register", "path":"/register"}
    ],   
    "models": [
        "Book"
    ],    
    "service": {
        "host": "localhost",
        "port": 3002,
        "name": "yb-test",
        "version": "1.0.0"
    }
}
```


