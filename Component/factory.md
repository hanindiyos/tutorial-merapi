# Factory 

**Factory** is a software design pattern which will return us an object.
In factory we allow to create an object without detailing to construct our object.

Instead of we using the *"new"* operator with the constructor, we better just calling the function which will return the object.


```javascript
    // logger
    di.register('logger', {factory: function($meta, logManager) {
        return logManager.createLogger($meta.caller);
    }});
```

In that example, we can look that the factory function will return of object that given by *createLogger* function in *logManager* file.


```javascript
// lib/log_manager.js
function createLogger(component) {        
        return logger.child({
            component:component,
            hostname: host,
            pid: port,
            level: level
       });
    }
```

With this code, we can create an object logger for a component.
