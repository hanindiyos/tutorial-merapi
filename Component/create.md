# What is Component 

The word object tends to mean something different to everyone. An object is a piece of compiled code that provides some function or method to the rest of the system. In [Merapi](../MerapiService/what.html), **Component** is a module which will return a JavaScript object. **A component** in merapi will have many function and this function will return a variable which are method of object. Component is an object which we can call and manipulated. Component have many function or method, so we can call they in our service or another service.

### How create a component

In this section we will create new component named *"hello_component.js"*

#### Step 1: Create Components Folder
First, we need to create a folder named components

![Local Image](../gitbook/images/Selection_010.png)

#### Step 2: Create Component File

Next, create file component named *"hello_component.js"* and copy the code below

```javascript
// hello_component.js
module.exports = function(config){
	var hello = config("hello");
	return {
		hello:function(world){
			console.log(hello+" "+world);
		}
	}
}
```
#### Step 3: Setting Configuration

Next step, after we make new component, we should initialize component to [config file](../Config/file.html)

```javascript
// config.json
{
    "hello" : "Hello",
    "components": [
        {"name":"helloComponent", "com":"helloComponent"}
    ],    
    "service": {
        "host": "localhost",
        "port": 4000,
        "name": "merapi-test",
        "version": "1.0.0"
    }
}
```
#### Step 4: Create File test.js and Run the Code

Last step, run these component with create test file.
Copy the code below and paste in *test.js*

```javascript
// test.js
var merapi = require('merapi');
var async = require('asyncawait/async');
var await = require('asyncawait/await');
var config = require('./config.json');

var container = merapi({
    basepath: __dirname,
	config: config
});

async(function(){
	await(container.initialize());
	var helloComponent = await(container.resolve("helloComponent"));
	await(helloComponent.hello("world!"));
})();
```

And if we run test.js, the result is 
```javascript
$ node test.js
Hello world!
```

Now, we know about **component** and we can **create a component**.
In the next section, we will learn about [factory](factory.html). 

