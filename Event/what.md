# What is Event 

**Event** is a result of action or signal that something had happened in service. Event is a component in [Merapi](../MerapiService/what.html) which the another service can subsribe it. The event can share the value to each of service which had [subscribe](subscribe.html) them. Thats means event is a *published subscribe*.

#### Step 1: Create Service 
In this example, we need to create a service name *"service1"*.

Look at the code below, we are create merapi service without an event.
```javascript
// service1.js
var merapi = require('merapi');
var async = require('asyncawait/async');
var await = require('asyncawait/await');
var service1 = merapi({
        basepath: __dirname,
        config: {
            service: {
                port: 3002,
                host: 'localhost',
                name: 'service1',
                version: '1.0.0'
            }
        }
    });
```
#### Step 2: Create Event in Service 
After we create our [merapi service](../MerapiService/what.html), we should create an event.
Next, we want to create event in service1, and the event named *"triggerX"*.

```javascript
//service1.js
var merapi = require('merapi');
var async = require('asyncawait/async');
var await = require('asyncawait/await');
var service1 = merapi({
        basepath: __dirname,
        config: {
	    // initialize event 
            components: [
                // created an event name triggerX
                {name:"triggerX", event:"test"}
            ],
            service: {
                port: 3002,
                host: 'localhost',
                name: 'service1',
                version: '1.0.0'
            }
        }
    });
```
We already created an event, so how can we use that?
we can do two things with event
###### [1. Trigger event](what.html)
###### [2. Subscribe event](subscribe.html)

Its so easy to trigger an event, here is the example how to trigger event
```javascript
// the trigger send the parameter "test" to the another service who subscribe the triggerX.
triggerX("test")
```

In the next section, we will learn about how to [subscribe event](subscribe.html).



