# How to Subscribe Event

To Subscribe other service's event, we must initialized the configuration in config file. 

In this example, we would show that how one service can subscribe the event from another service.

#### Step 1: Create Event in Service1 
First, we need to create an event in *service1*.
Service1 have an event named *"triggerX"*.

Copy the code below.
```javascript
// service1
var service1 = merapi({
        basepath: __dirname,
        config: {
	    // initialize the event 
            components: [
                // created an event name triggerX
                {name:"triggerX", event:"test"}
            ],
            service: {
                port: 3002,
                host: 'localhost',
                name: 'service1',
                version: '1.0.0'
            }
        }
    });
```

#### Step 2: Create Subscribe in Service2
Next, we need to **subscibe** event from *service1*.
So, service2 have subscribed event.

```javascript
// service2
  var service2 = merapi({
        basepath: __dirname,
        config: {
            logger: {level: 'error'},
            components: [
                {name:"proxy", proxy:"http://localhost:3002"}
            ],            
    // to initialize subscribe event
            subscriptions: [
                //service2 subscribe an event from service1
                {service:'http://localhost:3002', event:'test', hook:'/test'}
            ],            
            service: {
                port: 3001,
                host: 'localhost',
                name: 'test2',
                version: '1.0.0'
            }
        }
    });
```

#### Step 3: Create File test.js and Run the Code

After *service2* subscribe the event, we can trigger [event](what.html) in *service1* and look what will happend in *service2*.
And our *test.js* will be like this.


Copy the code below.
```javascript
// test.js
var merapi = require('merapi');
var async = require('asyncawait/async');
var await = require('asyncawait/await');
var service1 = merapi({
        basepath: __dirname,
        config: {
	    // initialize the event 
            components: [
                // created an event name triggerX
                {name:"triggerX", event:"test"}
            ],
            service: {
                port: 3002,
                host: 'localhost',
                name: 'service1',
                version: '1.0.0'
            }
        }
    });

var service2 = merapi({
        basepath: __dirname,
        config: {
            logger: {level: 'error'},
            components: [
                {name:"proxy", proxy:"http://localhost:3002"}
            ],            
    // to initialize subscribe event
            subscriptions: [
                //service2 subscribe an event from service1
                {service:'http://localhost:3002', event:'test', hook:'/test'}
            ],            
            service: {
                port: 3001,
                host: 'localhost',
                name: 'test2',
                version: '1.0.0'
            }
        }
    });

    async(function(){
        await(service1.start(function(triggerX) { 
            eventTrigger = triggerX;
            // trigger an event an send "test" to service2
            eventTrigger("test");
            return {}            
        }));        
        await(service2.start(function(proxy) {
            proxyA = proxy;
            return {
                //parameter word will receive "test" from event
                'HOOK /test': async(function(word) {
                    console.log(word);
                    // will print "test" in console
                })
            };
        }));    
    }();
```

And if we run test.js, the result is 
```javascript
$ node test.js
"test"
```

If we add more trigger in our code 
```javascript
// test.js
async(function(){
        await(service1.start(function(triggerX) { 
            eventTrigger = triggerX;
            // trigger an event an send "test" to service2
            eventTrigger("test");
	    eventTrigger("test 2x");
	    eventTrigger("test 3x");
            return {}            
        }));        
        await(service2.start(function(proxy) {
            proxyA = proxy;
            return {
                //parameter word will receive "test" from event
                'HOOK /test': async(function(word) {
                    console.log(word);
                    // will print "test" in console
                })
            };
        }));    
    }();
```

It will give us the result
```javascript
$ node test.js
"test"
"test 2x"
"test 3x"
```
