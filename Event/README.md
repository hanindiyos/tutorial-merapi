# Event

In this chapter we will learn about Event and how we can subscribe another service's event.

###### [10.1. What is Event](what.html)
###### [10.2. Subscribe Another Service's Event](subscribe.html)

