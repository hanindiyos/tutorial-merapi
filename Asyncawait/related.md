# How Asyncawait Related to Promise
Now we know about what a [promise](promise.html) is.
The next thing we should learn is about what asyncawait is and how asyncawait related to [promise](promise.html).

Remembered the **promise.js** in the previous section 
```javascript
// promise.js
function sum(sum1,sum2,res){
    return new Promise(function(resolve, reject){
    	res = sum1+sum2;
     	resolve(res);
    })
}

sumData(1,2).then(function(res){
	// result of fucntion call as input in next function call
	return sumData(res,res);
}).then(function(res2){
	return sumData(res2,res2);
}).then(function(res3){
	return sumData(res3,res3);
}).then(function(res4){
	return sumData(res4,res4);
}).then(function(res5){
	return sumData(res5,res5);
}).then(function(res6){
	return sumData(res6,res6);
}).then(function(res7){
	console.log(res7);
});
```
When we running the code will give us the result
```javascript
$ node promise.js
192
```

What if we re-write the promise.js to asyncawait pattern?

###But What Asyncawait is?

**Async await** is an asynchronous operation in Node.js [JavaScript](https://www.javascript.com/) code, which allow you to solve the problem of ["Callback Hell"](http://callbackhell.com/). In this operation enables you to wait the results of function before continuing with the following statement. Under the hood async functions using Promises - this is why the async function will return with a [Promise](promise.html).

```javascript
// asyncawait.js
var async = require('asyncawait/async');
var await = require('asyncawait/await');

function sum(sum1,sum2,res){
    return new Promise(function(resolve, reject){
    	res = sum1+sum2;
     	resolve(res);
    })
};

async(function(){
    res = await(sum(1,2));
    res2 = await(sum(res,res));
    res3 = await(sum(res2,res2));
    res4 = await(sum(res3,res3));
    res5 = await(sum(res4,res4));
    res6 = await(sum(res5,res5));
    res7 = await(sum(res6,res6));
    console.log(res7);
})(); 
```
When we running the code will give us the result
```javascript
$ node asyncawait.js
192
```

The result of these two code is same. 
What we should know is the result of *"async"* is the **promise**, and it changes to result if we call *"await"*. 
If we want to call promise many times, we must call **"then"** function many times too, its different with async await.







