# What is Promise

What is Promise? 

**Promise** represents an operation that hasn't completed yet, but is expected in the future. The calling code can wait until that promise is fulfilled before executing the next step. **Promises** will hold the information about the status of the async operation and will notify us when the async operation *succeeds* or *fails*.

Let’s take a look if we re-write the nested [callback](what.html) in the previous section using Promises.
Here is the example code for promise

```javascript
// promise.js
function sumData(sum1,sum2,res){
    return new Promise(function(resolve, reject){
    	res = sum1+sum2;
     	resolve(res);
    })
}

sumData(1,2).then(function(res){
	// result of fucntion call as input in next function call
	return sumData(res,res);
}).then(function(res2){
	return sumData(res2,res2);
}).then(function(res3){
	return sumData(res3,res3);
}).then(function(res4){
	return sumData(res4,res4);
}).then(function(res5){
	return sumData(res5,res5);
}).then(function(res6){
	return sumData(res6,res6);
}).then(function(res7){
	console.log(res7);
});
```

When we running the code will give us the result

```javascript
$ node promise.js
192
```
The code above have same logic and result with the code before with [callback pattern](what.html). 
Promises make our code are now much more readable than before. With the **promise**, we can call the result of the function with [".then"](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise/then) properties after calling the function. The benefits of using promise is we don't need to continually nest functions, and adding new work in the middle is as simple as adding a few extra lines.So we don't need a nested function like we use [callback](what.html) pattern.

