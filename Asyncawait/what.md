# Why Asyncawait?

In the previous chapter, we [installed async](../MerapiService/create.html) to our application.
So what is **Async**?

In this chapter, we would like to understand what is async and await, but before learning about asyncawait, 
Let's take a look the code below.

```javascript
// callback.js
function sumData(num1,num2,res){
	res(num1+num2);
}

sumData(1,2, function(res){
	console.log(res);
}
console.log("test");
```
The code above is **callback**. We usually create a callback pattern to handling [asynchronous operation](https://blog.risingstack.com/asynchronous-javascript/). This callback function is invoked the result of sumData. This is an asynchronous operation because the sumData call is non-blocking and if you were to execute the above program, you will notice that *"test"* will be printed before the result of sumData.

```javascript
$ node callback.js
test
3
```

If we run the code above, it is still good and easy to read. But what if we want to call sumData function and use the **callback** many times?. 
If you worked with [JavaScript](https://www.javascript.com/), maybe you will face any callback in your code, and it's possible you might facing nested callback in nested callback. When we want to call sumData and we want to use our **callback** as our input in the next function call, we must make the nested callback.

Here take a look the code below.

```javascript
// callbackhell.js
function sumData(num1,num2,res){
	res(num1+num2);
}

sumData(1,2, function(res){
	sumData(res,res,function(res2){
		sumData(res2,res2, function(res3){
			sumData(res3,res3, function(res4){
				sumData(res4,res4, function(res5){
					sumData(res5,res5, function(res6){
						sumData(res6,res6, function(res7){
							console.log(res7);
						});
					});
				});
			});
		});
	});
});
```
When we running the code will give us the result

```javascript
$ node callbackhell.js
192
```

  
That's will be a big problem if we code with callback pattern, we will have a lot of nested functions, and this is we called ["Callback Hell"](http://callbackhell.com/).

There is an alternative solution to handling asynchronous operation. And the solution called [promise](promise.html). 

In the next section, we will learn about [Promise](promise.html) pattern. 









