# Config Environment

As we know, in the application that we want to build , it must have some environment [configuration](file.html). The type of environment are **development, staging and production**. We also can set the current environment with one of the environment type that we have. 

If we have many environment, we need to make some config file to represent our environment.

1. **config.dev.json** will represents our development environment

2. **config.staging.json** will represent our staging environment

3. **config.prod.json** will represents our production environment

The *config.json* is the main **config file**. If we cant find the configuration in our config environment, the [Merapi](../MerapiService/what.html) will find the config in *config.json*.
Lets say, our environment now is development, so we used config.dev.json as our config file. 

Here is our config.dev.json

```javascript
// config.dev.json
{
	"db" : {
		"database" : "test"
	}, 
    "service": {
        "host": "localhost"
    }
}
``` 

We can find that, the config.dev.json doesnt contain any component or [controllers](../ModelController/controller.html) or [models](../ModelController/model.html). The Merapi cant find these component in there, so Merapi will find the configuration in config.json. 

```javascript 
// config.json
// in config.json Merapi can find the component
{
    "components": [
        {"name":"testWorld", "com":"TestWorld"}
    ], 
    "hello" : "world", 
    "service": {
        "host": "localhost",
        "port": 3001,
	// service.name
        "name": "merapi-test",
        "version": "1.0.0"   
}
```

Lets say we are in *development environment*, and we want to call this code 

```javascript
// hello_world.js
module.exports = function(config){	
	return {
		hello:function(){
			console.log(config('hello')+" "+config('service.name'));
		}
	}
}
```

We know in config.dev.json, we cant find *"hello"*.
So [Merapi](../MerapiService/what.html) will find the *"hello"* in main config file, config.json. So does *"service.name"*, altough config.dev.json have object *"service"* but we just can find the *"host"*. Because of that, Merapi will find "service.name" in config.json.

And the result of these code if we call the function *"helloWorld.hello"* is

```javascript
"hello merapi-test"
```







