# Config Files

**Config file** is configuration setting file for application which needed to use different values between your environment. Applications need some kind of configuration. There are different settings you might want to change depending on the application environment like setting service configuration, setting the database, and other such environment-specific things. And each configuration file will consist of one thing, returning an object. An application usually need some configuration type such as **development, production, staging and etc**.

The following configuration values are used internally by [Merapi](../MerapiService/what.html):

| Configuration |   Description            | 
| ------------- |:-------------| 
| Controllers   | Controller represents a file that connected client (view) and service.| 
| Models        | Model file defines a programming interface for interacting with the database (read, insert, update, etc)| 
| Components    | Component is a module which will return a JavaScript object. A component in merapi will have many function and this function will return a variable which are method of object.    | 
| Db            | DB represents our database location which we use for the application      | 
| Service       | Service impelements a part of applications. Service represents all operations that operate on entities/resources of a given part.  Each service should have only a small set of responsibilities |




This is an example of config.json contain configuration setting for the application. Config.json will loads the default configuration file in the application. We can create the config file in root folder


```javascript
// config.json
{
    "controllers": [
	    {"name":"register", "path":"/"}
	],    
    "models": [
	    " Book",
	    "Customer"	
	],    
    "db": {
	    "host": "localhost",
        "port": 30001
     },    
    "service": {
        "host": "localhost",
        "port": 3001,
        "name": "merapi-test",
        "version": "1.0.0"
    }
}
```

If we created some **component** in our folder, then we must add **the components** in *config.json*

```javascript
// config.json
{
    "components": [
        {"name":"helloWorld", "com":"HelloWorld"},
        {"name":"testWorld", "com":"TestWorld"},
        {"name":"finalWorld", "com":"FinalWorld"}
    ],    
    "controllers": [
        {"name":"register", "path":"/"}
    ],    
    "models": [
        "Book",
        "Customer"  
    ],    
    "db": {
        "host": "localhost",
        "port": 30001
     },    
    "service": {
        "host": "localhost",
        "port": 3001,
        "name": "merapi-test",
        "version": "1.0.0"
    }
}
```

If you add more component or controller in your service , you must initiliaze it in config.json to make it work in your application.
